import { AuthedContext } from '@middlewares/auth/context';
import { Event, EventInput, EventUpload } from '@modules/events/entities/Event';
import { User } from '@modules/users/entities/User';
import { UserRepository } from '@modules/users/repository/UserRepository';
import { UserResponse } from '@utils/context';
import logger from '@utils/logger';
import { getTimeAfter } from '@utils/time';
// import { Response } from 'express';
import { Arg, Args, Authorized, Ctx, Mutation, Query, Resolver } from 'type-graphql';
import { getManager, getRepository } from 'typeorm';
import { TrackingEvent, TrackingEventInput } from '../entities/TrackingEvent';
import { EventRepository } from '../repository/EventRepository';
import { TrackingEventRepository } from '../repository/trackingEventRepository';
// import { authTokenMiddleware } from '@middlewares/auth/authTokenMiddleware';

@Resolver()
export class EventResolver {
  private eventRepository = getRepository(Event);
  private trackingEventRepository = getRepository(TrackingEvent);
  private userEventRepository = getRepository(User);

  @Query(() => [Event])
  async getAllEvents(): Promise<Event[]> {
    const events = await this.eventRepository
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.vouchers', 'voucher')
      .getMany();
    return events;
    // return await this.eventRepository.find({});
  }

  @Query(() => Event)
  async getEvent(@Arg('id') id: string): Promise<Event> {
    const event = await this.eventRepository.findOne({ id });
    if (event) return event;
    logger.error('Event not found');
    throw new Error('Event not found');
  }

  @Mutation(() => Event)
  async addEvent(@Arg('data') data: EventInput): Promise<Event> {
    const event = await this.eventRepository.save({ ...data, voucherRemaining: data.maxQuantityVoucher });
    return event;
  }

  @Mutation(() => String)
  async updateEvent(@Args() { id, data }: EventUpload): Promise<string> {
    const result = await this.eventRepository.update({ id }, data);
    if (result.affected === 0) throw new Error('Event not found');
    return 'Update successfully';
  }

  // @Authorized()
  // @Mutation(() => String)
  // async eventEditable(@Arg('eventId') eventId: string, @Ctx() ctx: AuthedContext): Promise<string> {
  //   const event = await this.eventRepository.findOne({ id: eventId });
  //   const userId = ctx.req.auth?.userId;
  //   const user = await this.userEventRepository.findOne({ id: userId });
  //   if (event && user) {
  //     const trackingEvent = await this.trackingEventRepository
  //       .createQueryBuilder('tracking_event')
  //       .leftJoinAndSelect('tracking_event.userEditing', 'user')
  //       .leftJoinAndSelect('tracking_event.eventId', 'event')
  //       .where('tracking_event.eventId = :eventId', { eventId })
  //       .getOne();
  //     if (!trackingEvent) {
  //       const data: TrackingEventInput = {
  //         editable: false,
  //         eventId: event,
  //         userEditing: user,
  //         validTo: getTimeAfter(5, 'm'),
  //       };
  //       await this.trackingEventRepository.save(data);
  //     } else {
  //       const now = new Date().getTime();
  //       if (trackingEvent.validTo > now) {
  //         //ok
  //         if (trackingEvent.userEditing.id == userId) {
  //           ctx.res.status(200);
  //           return 'Event still editable';
  //         } else {
  //           ctx.res.status(409);
  //           return 'Event can not edited';
  //         }
  //       } else {
  //         //timeout
  //         const updateTracking = await this.trackingEventRepository.update(
  //           { id: trackingEvent.id },
  //           { validTo: getTimeAfter(5, 'm'), userEditing: user },
  //         );
  //         if (updateTracking) {
  //           ctx.res.status(200);
  //           return 'Event still editable';
  //         } else {
  //           ctx.res.status(409);
  //           return 'Event can not edited';
  //         }
  //       }
  //     }
  //     const countTrackingEvent = await this.trackingEventRepository.count({
  //       eventId: event,
  //     });
  //     if (countTrackingEvent > 1) {
  //       await this.trackingEventRepository.delete({ eventId: event });
  //       ctx.res.status(409);
  //       return 'Try request again';
  //     }
  //     ctx.res.status(200);
  //     return 'Event still editable';
  //   } else {
  //     ctx.res.status(404);
  //     return 'Event or user not found';
  //   }
  // }

  @Authorized()
  @Mutation(() => UserResponse)
  async eventEditable(@Arg('eventId') eventId: string, @Ctx() ctx: AuthedContext): Promise<UserResponse> {
    let result = new UserResponse();
    await getManager().transaction('SERIALIZABLE', async (transactionalEntityManager) => {
      const eventRepo = transactionalEntityManager.getCustomRepository(EventRepository);
      const userRepo = transactionalEntityManager.getCustomRepository(UserRepository);
      const trackingRepo = transactionalEntityManager.getCustomRepository(TrackingEventRepository);
      const event = await eventRepo.findOne({ id: eventId });
      const userId = ctx.req.auth?.userId;
      const user = await userRepo.findOne({ id: userId });
      if (event && user) {
        const trackingEvent = await trackingRepo
          .createQueryBuilder('tracking_event')
          .leftJoinAndSelect('tracking_event.userEditing', 'user')
          .leftJoinAndSelect('tracking_event.eventId', 'event')
          .where('tracking_event.eventId = :eventId', { eventId })
          .getOne();
        if (!trackingEvent) {
          const data: TrackingEventInput = {
            editable: false,
            eventId: event,
            userEditing: user,
            validTo: getTimeAfter(5, 'm'),
          };
          await trackingRepo.save(data);
        } else {
          const now = new Date().getTime();
          if (trackingEvent.validTo > now) {
            //ok
            if (trackingEvent.userEditing.id == userId) {
              result = {
                code: 200,
                success: true,
                message: 'Event still editable',
              };
            } else {
              result = {
                code: 409,
                success: false,
                message: 'Event can not edited',
              };
            }
          } else {
            //timeout
            const updateTracking = await trackingRepo.update(
              { id: trackingEvent.id },
              { validTo: getTimeAfter(5, 'm'), userEditing: user },
            );
            if (updateTracking.affected === 0) {
              result = {
                code: 409,
                success: false,
                message: 'Event can not edited',
              };
            } else {
              result = {
                code: 200,
                success: true,
                message: 'Event still editable',
              };
            }
          }
        }
      } else {
        result = {
          code: 404,
          success: false,
          message: 'Event or user not found',
        };
      }
    });
    return result;
  }

  @Authorized()
  @Mutation(() => UserResponse)
  async eventReleaseEdit(@Arg('eventId') eventId: string): Promise<UserResponse> {
    let result = new UserResponse();
    await getManager().transaction('SERIALIZABLE', async (transactionalEntityManager) => {
      const eventRepo = transactionalEntityManager.getCustomRepository(EventRepository);
      const trackingRepo = transactionalEntityManager.getCustomRepository(TrackingEventRepository);
      const event = await eventRepo.findOne({ id: eventId });
      if (!event)
        result = {
          code: 404,
          success: false,
          message: 'Event  not found',
        };
      const trackingDeleted = await trackingRepo.delete({ eventId: event });
      if (trackingDeleted.affected === 0) {
        result = {
          code: 409,
          success: false,
          message: 'Release failed',
        };
      } else {
        result = {
          code: 200,
          success: true,
          message: 'Release successfully',
        };
      }
    });
    return result;
  }

  @Authorized()
  @Mutation(() => UserResponse)
  async eventMaintainEdit(@Arg('eventId') eventId: string, @Ctx() ctx: AuthedContext): Promise<UserResponse> {
    let result = new UserResponse();
    await getManager().transaction('SERIALIZABLE', async (transactionalEntityManager) => {
      const eventRepo = transactionalEntityManager.getCustomRepository(EventRepository);
      const userRepo = transactionalEntityManager.getCustomRepository(UserRepository);
      const trackingRepo = transactionalEntityManager.getCustomRepository(TrackingEventRepository);
      const event = await eventRepo.findOne({ id: eventId });
      const userId = ctx.req.auth?.userId;
      const user = await userRepo.findOne({ id: userId });
      if (event && user) {
        const trackingEvent = await trackingRepo
          .createQueryBuilder('tracking_event')
          .leftJoinAndSelect('tracking_event.userEditing', 'user')
          .leftJoinAndSelect('tracking_event.eventId', 'event')
          .where('tracking_event.eventId = :eventId', { eventId })
          .getOne();
        if (trackingEvent) {
          const now = new Date().getTime();
          if (trackingEvent.userEditing.id == userId || trackingEvent.validTo < now) {
            const maintain = await trackingRepo.update(
              { id: trackingEvent.id },
              { validTo: getTimeAfter(5, 'm'), userEditing: user },
            );
            if (maintain) {
              result = {
                code: 200,
                success: true,
                message: 'Maintain successfully',
              };
            } else {
              result = {
                code: 400,
                success: false,
                message: 'Maintain failed',
              };
            }
          } else {
            result = {
              code: 400,
              success: false,
              message: 'Maintain failed',
            };
          }
        } else {
          const data: TrackingEventInput = {
            editable: false,
            eventId: event,
            userEditing: user,
            validTo: getTimeAfter(5, 'm'),
          };
          await trackingRepo.save(data);
          result = {
            code: 200,
            success: true,
            message: 'Maintain successfully',
          };
        }
      } else {
        result = {
          code: 404,
          success: false,
          message: 'Event or user not found',
        };
      }
    });
    return result;
  }

  @Authorized('ADMIN')
  @Mutation(() => String)
  async deleteEvent(@Arg('id') id: string): Promise<string> {
    const result = await this.eventRepository.delete({ id });
    if (result.affected === 0) throw new Error('Event not found');
    return 'Delete successfully';
  }
}
