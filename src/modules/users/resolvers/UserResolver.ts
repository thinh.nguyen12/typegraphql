import { AdminUpload } from './../entities/User';
import { getRepository } from 'typeorm';
import { Arg, Args, Authorized, Mutation, Query, Resolver } from 'type-graphql';
import logger from '@utils/logger';
import { User, UserInput, UserUpload } from '@modules/users/entities/User';

@Resolver()
export class UserResolver {
  private userRepository = getRepository(User);

  @Query(() => [User])
  async getAll(): Promise<User[]> {
    return await this.userRepository.find({});
  }

  @Query(() => User)
  async getOne(@Arg('id') id: string): Promise<User> {
    const user = await this.userRepository.findOne({ id });
    if (user) return user;
    logger.error('User not found');
    throw new Error('User not found');
  }

  @Mutation(() => User)
  async addOne(@Arg('data') data: UserInput): Promise<User> {
    const user = await this.userRepository.save(data);
    return user;
  }

  @Mutation(() => String)
  async updateOne(@Args() { id, data }: UserUpload): Promise<string> {
    const result = await this.userRepository.update({ id }, data);
    if (result.affected === 0) throw new Error('User not found');
    return 'Update successfully';
  }

  @Authorized('ADMIN')
  @Mutation(() => String)
  async deleteOne(@Arg('id') id: string): Promise<string> {
    const result = await this.userRepository.delete({ id });
    if (result.affected === 0) throw new Error('User not found');
    return 'Delete successfully';
  }

  @Authorized('ADMIN')
  @Mutation(() => String)
  async updateHiddenField(@Args() { id, hiddenField }: AdminUpload): Promise<string> {
    const result = await this.userRepository.update({ id }, { hiddenField });
    if (result.affected === 0) throw new Error('User not found');
    return hiddenField;
  }
}
