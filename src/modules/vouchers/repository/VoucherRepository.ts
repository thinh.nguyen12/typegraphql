import { EntityRepository, Repository } from 'typeorm';
import { Voucher } from '@modules/vouchers/entities/Voucher';

@EntityRepository(Voucher)
export class VoucherRepository extends Repository<Voucher> {}
