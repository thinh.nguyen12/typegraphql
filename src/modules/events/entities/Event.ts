import { BaseEntity } from '@customtypes/BaseEntity';
import { Voucher } from '@modules/vouchers/entities/Voucher';
import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';
import { Column, Entity, OneToMany } from 'typeorm';
import { TrackingEvent } from './TrackingEvent';

@Entity()
@ObjectType()
export class Event extends BaseEntity {
  @Field()
  @Column({
    type: 'varchar',
    length: 200,
  })
  name!: string;

  @Field()
  @Column({
    type: 'date',
  })
  startDate!: string;

  @Field()
  @Column({
    type: 'date',
  })
  endDate!: string;

  @Field()
  @Column({
    type: 'integer',
  })
  maxQuantityVoucher!: number;

  @Field()
  @Column({
    type: 'integer',
  })
  voucherRemaining!: number;

  @Field(() => [Voucher])
  @OneToMany(() => Voucher, (voucher: Voucher) => voucher.event)
  public vouchers?: Voucher[];

  @Field(() => [TrackingEvent], { nullable: true })
  @OneToMany(() => TrackingEvent, (trackingEvent: TrackingEvent) => trackingEvent.eventId)
  trackings?: TrackingEvent[];
}

@InputType()
export class EventInput implements Partial<Event> {
  @Field()
  name!: string;

  @Field()
  startDate!: string;

  @Field()
  endDate!: string;

  @Field()
  maxQuantityVoucher!: number;
}

@ArgsType()
export class EventUpload {
  @Field()
  id!: string;

  @Field(() => EventInput)
  data!: EventInput;
}
