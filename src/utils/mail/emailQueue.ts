import { Mail, State } from '@modules/mails/entities/Mail';
import { User } from '@modules/users/entities/User';
import { Voucher } from '@modules/vouchers/entities/Voucher';
import Queue from 'bull';
import { getRepository } from 'typeorm';
import { mailer } from './mailer';

const emailQueue = new Queue('send email', 'redis://127.0.0.1:6379');

export const addEmailToQueue = async (data: { voucher: Voucher; userId: string }): Promise<Queue.Job> => {
  const userRepository = getRepository(User);
  const user = await userRepository.findOne({ id: data.userId });
  return await emailQueue.add({ voucher: data.voucher, user }, { removeOnComplete: true });
};

emailQueue.on('error', function (err) {
  console.log('error', err);
});

emailQueue.on('failed', async (job, err) => {
  const mailRepository = getRepository(Mail);
  await mailRepository.update({ voucher: job.data.voucher, user: job.data.user }, { state: State.FAILED });
  console.log('failed', job, err);
});

emailQueue.on('completed', async (job, result) => {
  const mailRepository = getRepository(Mail);
  console.log('completed');

  await mailRepository.update({ voucher: job.data.voucher, user: job.data.user }, { state: State.SUCCESS });
  console.log(`Job completed with result ${result}`);
});

emailQueue.process(async (job, done) => {
  try {
    const mailRepository = getRepository(Mail);
    await mailRepository.save({
      voucher: job.data.voucher,
      user: job.data.user,
      state: State.PENDING,
    });
    const { email = 'admin@gmail.com' } = job.data.user,
      { code = '' } = job.data.voucher;
    await mailer(email, 'Voucher', code);
    console.log('<h3>Your email has been sent successfully.</h3>');
  } catch (error) {
    console.log(error);
  }
  done();
});
