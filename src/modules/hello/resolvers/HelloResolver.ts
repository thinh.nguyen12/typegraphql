import logger from '@utils/logger';
import { Query, Resolver } from 'type-graphql';

@Resolver()
export class HelloResolver {
  @Query(() => String)
  async hello(): Promise<string> {
    logger.error("This is an error log");
    logger.warn("This is a warn log");
    logger.info("This is a info log");
    return 'Hello world';
  }
}
