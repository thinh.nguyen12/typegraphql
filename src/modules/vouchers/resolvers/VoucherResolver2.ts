import { Event } from '@modules/events/entities/Event';
import { EventRepository } from '@modules/events/repository/EventRepository';
import { User } from '@modules/users/entities/User';
import { Voucher, VoucherInput, VoucherUpload } from '@modules/vouchers/entities/Voucher';
import logger from '@utils/logger';
import { makeVoucherCode } from '@utils/random';
import { ApolloError } from 'apollo-server-core';
import { Arg, Args, Authorized, Mutation, Query, Resolver } from 'type-graphql';
import { getManager, getRepository, MoreThan } from 'typeorm';
import { VoucherRepository } from '../repository/VoucherRepository';

@Resolver()
export class VoucherResolver {
  private voucherRepository = getRepository(Voucher);
  private eventRepository = getRepository(Event);
  private userRepository = getRepository(User);

  @Query(() => [Voucher])
  async getAllVoucher(): Promise<Voucher[]> {
    return await this.voucherRepository.find({});
  }

  @Query(() => Voucher)
  async getOneVoucher(@Arg('id') id: string): Promise<Voucher> {
    const voucher = await this.voucherRepository.findOne({ id });
    if (voucher) return voucher;
    logger.error('Voucher not found');
    throw new Error('Voucher not found');
  }

  @Mutation(() => Voucher)
  async getVoucherByEvent(@Arg('eventId') eventId: string): Promise<Voucher> {
    console.log('check');
    const event = await this.eventRepository.findOne({ id: eventId });
    if (!event) throw new Error('Event not found');
    if (event.voucherRemaining && event.voucherRemaining > 0) {
      const data: VoucherInput = { code: makeVoucherCode(5), event };
      const [oldVoucher, oldCountVoucher] = await this.voucherRepository.findAndCount({
        where: { event },
        order: {
          createdAt: 'DESC',
        },
      });
      console.log('oldVoucher', oldVoucher);
      await this.eventRepository.update(
        { id: event.id, voucherRemaining: MoreThan(0) },
        { voucherRemaining: event.voucherRemaining - 1 },
      );
      await this.voucherRepository.save(data);
      const [voucher, countVoucher] = await this.voucherRepository.findAndCount({
        where: { event },
        order: {
          createdAt: 'DESC',
        },
      });
      console.log('voucher', voucher);

      if (countVoucher - oldCountVoucher > 1) {
        const diff = await this.voucherRepository.count({
          event,
          createdAt: oldVoucher[0] && MoreThan(oldVoucher[0].createdAt),
        });
        console.log('diff', diff);
        await this.voucherRepository.delete({
          event,
          createdAt: oldVoucher[0] && MoreThan(oldVoucher[0].createdAt),
        });
        await this.eventRepository.update(
          { id: event.id },
          { voucherRemaining: event.maxQuantityVoucher - oldCountVoucher },
        );
        throw new Error('Get voucher failed');
      }
      return voucher[0];
    }
    throw new Error('Voucher sold out');
  }

  @Mutation(() => Voucher)
  async getVoucherByEventTrans(@Arg('eventId') eventId: string): Promise<Voucher> {
    let result = new Voucher();
    await getManager().transaction(async (transactionalEntityManager) => {
      console.log('check', new Date());
      const eventRepo = transactionalEntityManager.getCustomRepository(EventRepository);
      const event = await eventRepo.findOne({ id: eventId });
      if (!event) throw new ApolloError('Event not found');
      if (event.voucherRemaining < 1) {
        console.log('sold out');
        throw new ApolloError('Voucher sold out');
      } else {
        const eventUpdated = await eventRepo.update(
          { id: eventId, voucherRemaining: MoreThan(0) },
          { voucherRemaining: event.voucherRemaining - 1 },
        );
        if (eventUpdated) {
          const voucherRepo = transactionalEntityManager.getCustomRepository(VoucherRepository);
          const voucher = await voucherRepo.save({ code: makeVoucherCode(5), event });
          result = voucher;
        }
      }
    });
    //send email
    // const user = await this.userRepository.findOne({ id: ctx.req.auth?.userId });
    // if (user) {
    //   await addEmailToQueue({ voucher: result, user });
    // }
    return result;
  }

  @Mutation(() => Voucher)
  async addOneVoucher(@Arg('data') data: VoucherInput): Promise<Voucher> {
    const voucher = await this.voucherRepository.save(data);
    return voucher;
  }

  @Mutation(() => String)
  async updateOneVoucher(@Args() { id, data }: VoucherUpload): Promise<string> {
    const result = await this.voucherRepository.update({ id }, data);
    if (result.affected === 0) throw new Error('Voucher not found');
    return 'Update successfully';
  }

  @Authorized('ADMIN')
  @Mutation(() => String)
  async deleteOneVoucher(@Arg('id') id: string): Promise<string> {
    const result = await this.voucherRepository.delete({ id });
    if (result.affected === 0) throw new Error('Voucher not found');
    return 'Delete successfully';
  }
}
