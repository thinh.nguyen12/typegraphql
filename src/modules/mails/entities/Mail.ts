import { BaseEntity } from '@customtypes/BaseEntity';
import { User } from '@modules/users/entities/User';
import { Voucher } from '@modules/vouchers/entities/Voucher';
import { Field, InputType, ObjectType, registerEnumType } from 'type-graphql';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';

export enum State {
  PENDING = 'PENDING',
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}
registerEnumType(State, {
  name: 'State',
  description: 'State of email',
});

@Entity()
@ObjectType()
export class Mail extends BaseEntity {
  @Field(() => State)
  @Column({
    type: 'varchar',
    length: 30,
    default: State.PENDING,
  })
  state?: string;

  @Field(() => Voucher)
  @OneToOne(() => Voucher, (voucher: Voucher) => voucher.id)
  @JoinColumn({ name: 'voucherId' })
  voucher!: string;

  @Field(() => User)
  @ManyToOne(() => User, (user: User) => user.id)
  @JoinColumn({ name: 'userId' })
  user!: string;
}

@InputType()
export class MailInput implements Partial<Mail> {
  @Field(() => String)
  voucherId!: string;

  @Field(() => String)
  userId?: string;

  @Field(() => State)
  state!: string;
}
