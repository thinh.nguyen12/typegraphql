import { EntityRepository, Repository } from 'typeorm';
import { Event } from '@modules/events/entities/Event';

@EntityRepository(Event)
export class EventRepository extends Repository<Event> {}
