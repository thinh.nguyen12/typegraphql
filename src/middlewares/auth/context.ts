import { Request, Response } from 'express';

export interface AuthInfo {
  userId: string;
  role?: string;
}

export interface AuthedRequest extends Request {
  auth?: AuthInfo;
}

export class AuthedContext {
  public constructor(public readonly req: AuthedRequest, public readonly res: Response) {}
}
