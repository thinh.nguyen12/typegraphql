import { buildSchema } from 'type-graphql';
import { GraphQLSchema } from 'graphql';
import { authChecker } from '@middlewares/auth/authChecker';
import { HelloResolver } from '@modules/hello/resolvers/HelloResolver';
import { UserResolver } from '@modules/users/resolvers/UserResolver';
import { AuthResolver } from '@modules/auth/resolvers/AuthResolver';
import { EventResolver } from '@modules/events/resolvers/EventResolver';
import { VoucherResolver } from '@modules/vouchers/resolvers/VoucherResolver';
import { MailResolver } from '@modules/mails/resolvers/MailResolver';

export const createGraphqlSchema = async (): Promise<GraphQLSchema> => {
  return buildSchema({
    resolvers: [HelloResolver, UserResolver, AuthResolver, EventResolver, VoucherResolver, MailResolver],
    authChecker: authChecker,
  });
};
