import { BaseEntity } from '@customtypes/BaseEntity';
import { Event } from '@modules/events/entities/Event';
import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity()
@ObjectType()
export class Voucher extends BaseEntity {
  @Field()
  @Column({
    type: 'varchar',
    length: 10,
  })
  code!: string;

  @Field()
  @Column({ type: 'varchar' })
  @Field(() => Event)
  public eventId!: string;

  @ManyToOne(() => Event, (event: Event) => event.vouchers)
  @JoinColumn({ name: 'eventId' })
  public event!: Event;
}

@InputType()
export class VoucherInput implements Partial<Voucher> {
  @Field()
  code!: string;

  @Field(() => String)
  event!: Event;
}

@ArgsType()
export class VoucherUpload {
  @Field()
  id!: string;

  @Field(() => VoucherInput)
  data!: VoucherInput;
}
