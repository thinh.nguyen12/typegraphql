import { Field, ObjectType } from 'type-graphql';
import { CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
@ObjectType()
export class BaseEntity {
  @Field()
  // @PrimaryColumn({ unique: true, default: uniqueId() })
  @PrimaryGeneratedColumn()
  id!: string;

  @Field()
  @CreateDateColumn()
  createdAt!: Date;

  @Field()
  @UpdateDateColumn()
  updatedAt!: Date;
}
