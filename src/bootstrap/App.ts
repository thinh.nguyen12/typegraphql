import http, { Server } from 'http';
import express, { Express } from 'express';
import cookieParser from 'cookie-parser';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import { createConnection } from 'typeorm';
import expressWinston from 'express-winston';
import config from '@config/index';
import { loggerConfig } from '@utils/logger';
import { createGraphqlSchema } from '@bootstrap/createGraphqlSchema';
import { AuthedContext, AuthedRequest } from '@middlewares/auth/context';
import { authTokenMiddleware } from '@middlewares/auth/authTokenMiddleware';

// @see https://www.apollographql.com/docs/apollo-server/integrations/middleware/#apollo-server-express
export class App {
  public readonly port: number;
  public readonly app: Express;
  public readonly httpServer: Server;

  public constructor() {
    this.port = config.PORT;
    this.app = express();
    this.httpServer = http.createServer(this.app);
  }

  protected async bootstrap(): Promise<void> {
    this.middlewares();
    await createConnection();
    await this.startApolloServer();
  }

  protected middlewares(): void {
    this.app.use(cookieParser());
    this.app.use(expressWinston.logger(loggerConfig));
    this.app.use(authTokenMiddleware);
  }

  protected async startApolloServer(): Promise<void> {
    const schema = await createGraphqlSchema();
    const apolloServer = new ApolloServer({
      schema,
      plugins: [ApolloServerPluginDrainHttpServer({ httpServer: this.httpServer })],
      context: ({ req, res }): AuthedContext => new AuthedContext(req as AuthedRequest, res),
    });
    await apolloServer.start();
    apolloServer.applyMiddleware({ app: this.app, cors: true });
  }

  public async start(): Promise<void> {
    try {
      await this.bootstrap();
      await new Promise((resolve) => {
        this.httpServer.listen(this.port, () => resolve(true));
      });
      console.log(`Server started on http://localhost:${this.port}/graphql`);
    } catch (error) {
      console.log('Start error: ', error);
    }
  }
}
