import { EntityRepository, Repository } from 'typeorm';
import { User } from '@modules/users/entities/User';

@EntityRepository(User)
export class UserRepository extends Repository<User> {}
