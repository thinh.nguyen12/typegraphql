import { User } from '@modules/users/entities/User';
import { sign } from 'jsonwebtoken';
import config from '@config/index';

export const createToken = (user: User): string =>
  sign(
    {
      userId: user.id,
    },
    config.ACCESS_TOKEN_SECRET,
    { expiresIn: '24h' },
  );
