import { AuthedContext } from '@middlewares/auth/context';
import { EventRepository } from '@modules/events/repository/EventRepository';
import { Voucher } from '@modules/vouchers/entities/Voucher';
import { addEmailToQueue } from '@utils/mail/emailQueue';
import { makeVoucherCode } from '@utils/random';
import { ApolloError } from 'apollo-server-core';
import { Arg, Authorized, Ctx, Mutation, Resolver } from 'type-graphql';
import { getManager } from 'typeorm';
import { VoucherRepository } from '../repository/VoucherRepository';
@Resolver()
export class VoucherResolver {
  @Authorized()
  @Mutation(() => Voucher)
  async getVoucherByEvent(@Arg('eventId') eventId: string, @Ctx() ctx: AuthedContext): Promise<Voucher> {
    let result = new Voucher();
    await getManager().transaction('SERIALIZABLE', async (transactionalEntityManager) => {
      const eventRepo = transactionalEntityManager.getCustomRepository(EventRepository);
      const voucherRepo = transactionalEntityManager.getCustomRepository(VoucherRepository);
      const event = await eventRepo.findOne({ id: eventId });
      if (!event) throw new ApolloError('Event not found');
      const eventUpdated = await eventRepo.update({ id: eventId }, { voucherRemaining: event.voucherRemaining - 1 });
      if (eventUpdated.affected === 0) throw new ApolloError('Error update event');
      const eventSaved = await eventRepo.findOne({ id: eventId });
      if (!eventSaved || eventSaved?.voucherRemaining >= event.voucherRemaining)
        throw new ApolloError('Error count voucher');
      if (!eventSaved || eventSaved.voucherRemaining < 0) {
        console.log('sold out');
        throw new ApolloError('Voucher sold out');
      }
      const voucherSaved = await voucherRepo.save({ code: makeVoucherCode(5), eventId: event.id });
      if (!voucherSaved) throw new ApolloError('Error save voucher');
      result = voucherSaved;
    });
    if (ctx.req.auth && ctx.req.auth.userId) addEmailToQueue({ voucher: result, userId: ctx.req.auth?.userId });
    return result;
  }
}
