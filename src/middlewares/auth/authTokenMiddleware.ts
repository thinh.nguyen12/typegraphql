import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';

import { AuthedRequest, AuthInfo } from './context';
import config from '@config/index';

interface Tokens {
  accessToken?: string;
  refreshToken?: string;
}

export const readTokens = (req: AuthedRequest): Tokens => {
  const cookies = req.cookies as Record<string, string>;
  let refreshToken: string | undefined = cookies[`refresh-token-${config.env}`];
  let accessToken: string | undefined = cookies[`access-token-${config.env}`];

  // fallback for reading access and refresh token
  if (!refreshToken && !accessToken) {
    accessToken = req.header('Authorization');
    refreshToken = req.header('Authorization Refresh');
    if (accessToken) {
      accessToken = accessToken.split(' ')[1];
    }
    if (refreshToken) {
      refreshToken = refreshToken.split(' ')[1];
    }
  }
  return { accessToken, refreshToken };
};

export const authTokenMiddleware = async (ureq: Request, res: Response, next: NextFunction): Promise<void> => {
  const req = ureq as unknown as AuthedRequest;
  const tokens = readTokens(req);
  if (!tokens.accessToken) {
    return next();
  }
  try {
    const decoded = verify(tokens.accessToken, config.ACCESS_TOKEN_SECRET) as AuthInfo;
    req.auth = decoded;
  } catch (e) {
    console.log('Auth error: ', e);
  }
  next();
};
