import { BaseEntity } from '@customtypes/BaseEntity';
import { User } from '@modules/users/entities/User';
import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';
import { Column, Entity, ManyToOne, JoinColumn } from 'typeorm';
import { Event } from './Event';

@Entity()
@ObjectType()
export class TrackingEvent extends BaseEntity {
  @Field()
  @Column({
    type: 'boolean',
  })
  editable!: boolean;

  @Field()
  @Column({
    type: 'bigint',
  })
  validTo!: number;

  @Field(() => User)
  @ManyToOne(() => User, (user: User) => user.id)
  @JoinColumn({ name: 'userEditing' })
  userEditing!: User;

  @Field(() => Event)
  @ManyToOne(() => Event, (event: Event) => event.id)
  @JoinColumn({ name: 'eventId' })
  eventId!: Event;
}

@InputType()
export class TrackingEventInput implements Partial<TrackingEvent> {
  @Field()
  editable!: boolean;

  @Field()
  validTo!: number;

  @Field(() => String)
  userEditing!: User;

  @Field(() => String)
  eventId!: Event;
}

@ArgsType()
export class TrackingEventUpload {
  @Field()
  id!: string;

  @Field(() => TrackingEventInput)
  data!: TrackingEventInput;
}
