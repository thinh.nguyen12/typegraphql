import { Event } from '@modules/events/entities/Event';
import { uniqueId } from '@utils/uniqueId';
import Faker from 'faker';
import { define } from 'typeorm-seeding';

define(Event, (faker: typeof Faker) => {
  const event = new Event();
  event.id = uniqueId();
  event.name = faker.name.title();
  event.startDate = faker.date.weekday();
  event.endDate = faker.date.weekday();
  return event;
});
