'use strict';
const modules = require('module-alias');
const path = require('path');
const root = path.join(process.cwd(), 'dist');

modules.addAliases({
  '@utils': path.join(root, 'utils'),
  '@bootstrap': path.join(root, 'bootstrap'),
  '@middlewares': path.join(root, 'middlewares'),
  '@config': path.join(root, 'config'),
  '@customtypes': path.join(root, 'types'),
  '@modules': path.join(root, 'modules'),
});

require('./dist');
