import { BaseEntity } from '@customtypes/BaseEntity';
import { TrackingEvent } from '@modules/events/entities/TrackingEvent';
import { Mail } from '@modules/mails/entities/Mail';
import { ArgsType, Authorized, Field, InputType, ObjectType, registerEnumType } from 'type-graphql';
import { Column, Entity, OneToMany } from 'typeorm';

export enum Roles {
  ADMIN = 'ADMIN',
  CLIENT = 'CLIENT',
}
registerEnumType(Roles, {
  name: 'Roles',
  description: 'Roles of user',
});

@Entity()
@ObjectType()
export class User extends BaseEntity {
  @Field()
  @Column({
    type: 'varchar',
    length: 50,
  })
  lastName!: string;

  @Field()
  @Column({
    type: 'varchar',
    length: 50,
  })
  firstName!: string;

  @Field()
  @Column({
    type: 'varchar',
    length: 50,
    unique: true,
  })
  email!: string;

  @Field()
  @Column({
    type: 'varchar',
    length: 200,
  })
  password!: string;

  @Field(() => Roles, { nullable: true })
  @Column({
    type: 'varchar',
    length: 30,
    default: Roles.CLIENT,
  })
  roles?: string;

  @Authorized(Roles.ADMIN)
  @Field({ nullable: true })
  @Column({
    type: 'varchar',
    length: 50,
    default: '',
  })
  hiddenField?: string;

  @Field(() => [TrackingEvent], { nullable: true })
  @OneToMany(() => TrackingEvent, (trackingEvent: TrackingEvent) => trackingEvent.eventId)
  trackings?: TrackingEvent[];

  @Field(() => [Mail], { nullable: true })
  @OneToMany(() => Mail, (mail: Mail) => mail.id)
  mails?: Mail[];
}

@InputType()
export class UserInput implements Partial<User> {
  @Field()
  firstName!: string;

  @Field()
  lastName!: string;

  @Field()
  email!: string;

  @Field()
  password!: string;

  @Field(() => Roles, { nullable: true })
  roles?: string;
}

@ArgsType()
export class UserUpload {
  @Field()
  id!: string;

  @Field(() => UserInput)
  data!: UserInput;
}

@ArgsType()
export class AdminUpload {
  @Field()
  id!: string;

  @Field()
  hiddenField!: string;
}
