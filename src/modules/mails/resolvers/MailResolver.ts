import { Mail } from '@modules/mails/entities/Mail';
import { Query, Resolver } from 'type-graphql';
import { getRepository } from 'typeorm';

@Resolver()
export class MailResolver {
  private mailRepository = getRepository(Mail);

  @Query(() => [Mail])
  async getAll(): Promise<Mail[]> {
    return await this.mailRepository.find({});
  }
}
