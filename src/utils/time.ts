export const getTimeAfter = (offset = 0, typeOffset = 's'): number => {
  const now = new Date().getTime();
  switch (typeOffset) {
    case 'ms':
      return now + offset;
    case 's':
      return now + offset * 1000;
    case 'm':
      return now + offset * 1000 * 60;
    case 'h':
      return now + offset * 1000 * 60 * 60;
    case 'd':
      return now + offset * 1000 * 60 * 60 * 24;
    default:
      return now;
  }
};
