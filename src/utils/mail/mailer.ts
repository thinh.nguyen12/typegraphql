import nodeMailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

const adminEmail = 'nguyendinhthinhdc2@gmail.com';
const adminPassword = 'Thinh1703';
const mailHost = 'smtp.gmail.com';
const mailPort = 587;

export const mailer = (to: string, subject: string, content: string): Promise<SMTPTransport.SentMessageInfo> => {
  const transporter = nodeMailer.createTransport({
    host: mailHost,
    port: mailPort,
    secure: false,
    auth: {
      user: adminEmail,
      pass: adminPassword,
    },
  });
  const options = {
    from: adminEmail,
    to: to,
    subject: subject,
    html: `<strong>Your voucher code is ${content}</strong>`,
  };
  return transporter.sendMail(options);
};
