import {MigrationInterface, QueryRunner} from "typeorm";

export class UserMigration implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query('ALTER TABLE "user" RENAME COLUMN "firstName" TO "givenName"');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query('ALTER TABLE "user" RENAME COLUMN "givenName" TO "firstName"');
    }

}
