import { ArgsType, Field, ObjectType } from 'type-graphql';
import { User } from '@modules/users/entities/User';

@ArgsType()
export class AuthPayLoad implements Partial<User> {
  @Field()
  email!: string;

  @Field()
  password!: string;
}

@ObjectType()
export class AuthResponse {
  @Field()
  accessToken!: string;
}
