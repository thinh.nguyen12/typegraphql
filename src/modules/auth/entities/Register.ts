import { ArgsType, Field, ObjectType } from 'type-graphql';
import { User } from '@modules/users/entities/User';

@ArgsType()
export class RegisterInput implements Partial<User> {
  @Field()
  email!: string;

  @Field()
  password!: string;

  @Field()
  firstName!: string;

  @Field()
  lastName!: string;
}

@ObjectType()
export class RegisterResponse {
  @Field()
  accessToken!: string;
}
