import { AuthedContext } from '@middlewares/auth/context';
import { AuthPayLoad, AuthResponse } from '@modules/auth/entities/Auth';
import { User } from '@modules/users/entities/User';
import { createToken } from '@utils/createToken';
import * as argon2 from 'argon2';
import { Args, Authorized, Ctx, Mutation, Query, Resolver } from 'type-graphql';
import { getRepository } from 'typeorm';
import { RegisterInput, RegisterResponse } from '../entities/Register';

@Resolver()
export class AuthResolver {
  private userRepository = getRepository(User);

  @Mutation(() => RegisterResponse)
  async register(@Args() registerInput: RegisterInput): Promise<RegisterResponse> {
    const { email, password, firstName, lastName } = registerInput;
    const user = await this.userRepository.findOne({ email });
    if (user) throw new Error('Email has already used');
    const hashPassword = await argon2.hash(password);
    const newUser = this.userRepository.create({
      email,
      password: hashPassword,
      firstName,
      lastName,
    });
    const userSaved = await this.userRepository.save(newUser);
    const accessToken = createToken(userSaved);
    return { accessToken };
  }

  @Mutation(() => AuthResponse)
  async login(@Args() { email, password }: AuthPayLoad): Promise<AuthResponse> {
    const user = await this.userRepository.findOne({ email });
    if (!user) throw new Error('Email does not exist');
    const isAuth = await argon2.verify(user.password, password);
    if (!isAuth) throw new Error('Password wrong');
    const accessToken = createToken(user);
    return { accessToken };
  }

  @Authorized()
  @Query()
  authedQuery(@Ctx() ctx: AuthedContext): string {
    console.log('AUTH: ', ctx.req.auth);
    return 'Authorized users only!';
  }

  @Authorized('ADMIN')
  @Mutation()
  adminMutation(): string {
    return 'You are an admin, you can safely drop the database';
  }
}
