import { Field, InterfaceType, ObjectType } from 'type-graphql';

@InterfaceType()
export abstract class IUserResponse {
  @Field()
  code!: number;

  @Field()
  success!: boolean;

  @Field({ nullable: true })
  message?: string;
}

@ObjectType({ implements: IUserResponse })
export class UserResponse implements IUserResponse {
  code!: number;
  success!: boolean;
  message?: string;
}
