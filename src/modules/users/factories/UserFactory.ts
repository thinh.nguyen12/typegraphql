import { User } from '@modules/users/entities/User';
import { uniqueId } from '@utils/uniqueId';
import Faker from 'faker'
import { define } from 'typeorm-seeding';

define(User, (faker: typeof Faker) => {
  const user = new User()
  user.id = uniqueId()
  user.firstName = faker.name.firstName()
  user.lastName = faker.name.lastName()
  user.email = `${faker.name.lastName()}@gmail.com`
  user.password = faker.name.lastName()
  return user
})
