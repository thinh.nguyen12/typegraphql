module.exports = {
  type: process.env.DB_TYPE,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true,
  entities: ['src/modules/**/entities/*{.ts,.js}'],
  migrations: ['dist/migrations/*.js}'],
  subscribers: ['src/modules/**/subscribers/*{.ts,.js}'],
  cli: {
    entitiesDir: ['src/modules/**/entities/*{.ts,.js}'],
    migrationsDir: 'src/migrations',
  },
  seeds: ['seeds/*{.ts,.js}'],
  factories: ['src/modules/**/factories/*{.ts,.js}'],
};
