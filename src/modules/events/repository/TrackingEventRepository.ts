import { EntityRepository, Repository } from 'typeorm';
import { TrackingEvent } from '@modules/events/entities/TrackingEvent';

@EntityRepository(TrackingEvent)
export class TrackingEventRepository extends Repository<TrackingEvent> {}
